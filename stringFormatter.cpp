// Type your code here, or load an example.
# include <iostream>
# include <type_traits>
# include <sstream>

template <typename Arg>
std::string format(const std::string& fmt, Arg arg)
{
    bool isArgFormatted = false;
    std::string fmtStr = "";
    std::size_t fmtSize = fmt.size();
    
    for ( std::size_t i = 0; i < fmtSize; i++ ) {
        if ((not isArgFormatted) and (i+1) < fmtSize and fmt[i] == '{' and fmt[i+1] == '}') {
            std::string argStringValue{};

            std::istringstream iss{arg};
            iss >> argStringValue;
            fmtStr += argStringValue;
            
            isArgFormatted = true;

        }
        else {
            char currentChar = fmt[i];
            
            if (not (currentChar == '}')) {
                fmtStr += fmt[i];
                continue;
            }

            fmtStr += fmt[i];
        }
    }

    return fmtStr;
}

template <typename Arg, typename... Rest>
std::string format(const std::string &fmt, Arg arg, Rest... rest)
{
    std::string fmtStr{};
    std::string formatStringCopy = fmt;
    std::size_t fmtSize = fmt.size();
    
    for ( std::size_t i = 0; i < fmtSize; i++ ) {

         if ((i+1) < fmtSize and fmt[i] == '{' and fmt[i+1] == '}') {
            fmtStr += std::to_string(arg);
            formatStringCopy = formatStringCopy.substr(i+2,fmtSize);
            fmtStr += format(formatStringCopy,rest...);
        }
        else {

            char currentChar = formatStringCopy[i];
            
            if (not (currentChar == '}')) {
                fmtStr += formatStringCopy[i];
            }
        }
    }

    return fmtStr;
}

int main() 
{
    std::string fmt = "My name is {}";
    std::cout << format(fmt,"Abdul Hameed");
}