// Type your code here, or load an example.
# include <iostream>
# include <memory>
# include <mutex>
# include <algorithm>
# include <vector>

template <typename ObjectType>
class MemoryPool
{
    public:

        [[ nodiscard ]] MemoryPool() = default;

        [[ nodiscard ]] MemoryPool(MemoryPool&& pool)
            : memoryPool(std::exchange(memoryPool,std::unordered_set<MemData>{}))
        {}

        MemoryPool(const MemoryPool& p) = delete;

        virtual ~MemoryPool() = default;

        MemoryPool operator=(const MemoryPool& pool) = delete;

        MemoryPool operator=(MemoryPool&& pool)
            : memoryPool(std::exchange(memoryPool,std::unordered_set<MemData>{}))
        {}

        template <typename... Args>
        [[ nodiscard ]] std::shared_ptr<ObjectType> construct(Args&&... args) {
            std::lock_gaurd<std::mutex> lock(mutex);
            auto allocatedObject = findAndConstructObject(std::forward<Args>(args) ...);

            if (allocatedObject == nullObject) {
                allocatedObject = new ObjectType{std::forward<Args>(args)...};
            }
            return allocatedObject;
        }

        void destroy(std::shared_ptr<ObjectType> objectPtr) {
            for (auto& mem : memoryPool) {
                if (mem == objectPtr) {
                    mem.is_allocated = false;
                }
            }
        }

    private:

        struct MemData {

            public:

                bool is_allocated = true;
                std::shared_ptr<ObjectType> pointer{};

                MemData() = default;

                MemData(std::shared_ptr<ObjectType> pointer)
                    : pointer(pointer)
                {}

                bool operator==(const MemData& m) {
                    return (m.is_allocated == is_allocated) and
                           (m.pointer == pointer);
                }

                bool operator!=(const MemData& m) {
                    return not operator==(m);
                }

        };

        const nullObject = std::shared_ptr<ObjectType>{};

        template <typename... Args>
        std::shared_ptr<ObjectType> findAndConstructObject(Args&&... args) {
            auto freeMem = nullObject;
            
            for (auto& mem : memoryPool) {
                if (not mem.is_allocated) {
                    mem.is_allocated = true;
                    freeMem = new (mem.pointer.get()) ObjectType{std::forward<Args>(args)...}; /* using placement new operator */
                }
            }

            return freeMem;
        }

        std::mutex mutex{};
        std::vector<MemData> memoryPool;
}